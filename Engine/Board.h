#pragma once

#include "Graphics.h"
#include "Location.h"

class Board {
public:
	Board(Graphics& gfx);//constructor for what?
	void DrawCell(const Location& loc, Color c);
	int GetHeight() const;
	int GetWidth() const;
	bool IsInsideBoard(const Location& loc) const;
	void DrawBorder();
	int GetDimension()const;

private:
	static constexpr int dimension = 20;//the dimension of the board i.e. 20x20.
	static constexpr int width = Graphics::ScreenWidth /dimension;
	static constexpr int height = Graphics::ScreenHeight/dimension;
	Graphics& gfx;//dont know
};