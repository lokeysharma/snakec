#include "Board.h"
#include <assert.h>

Board::Board(Graphics & gfx)
	:gfx(gfx)
{
}

void Board::DrawCell(const Location & loc, Color c)
{
	//using assetion in thisfor jut to chek in the debug mode not to fuckup.
	assert(loc.x >= 0);
	assert(loc.y >= 0);
	assert(loc.x < width);
	assert(loc.y < height);
	gfx.DrawRectDim(loc.x *dimension, loc.y*dimension, dimension, dimension, c);
}

int Board::GetHeight() const
{
	return height;
}

int Board::GetWidth() const
{
	return width;
}

bool Board::IsInsideBoard(const Location & loc) const
{
	return loc.x >= 1 && loc.x < width-1 && loc.y >= 1 && loc.y < height-1;
}

void Board::DrawBorder()
{
	int height = GetHeight() * dimension;
	int width = GetWidth()* dimension;
	Color c = Colors::LightGray;
	// for up wall
	for (int i = 15; i < width-15; i++)
	{
		for (int j = 8; j < 15; j++)
		{
			gfx.PutPixel(i, j, c);
		}
	}

	// for left wall
	for (int i = 8; i < 15; i++)
	{
		for (int j = 8; j < height-8; j++)
		{
			gfx.PutPixel(i, j, c);
		}
	}

	// for right wall
	for (int i = width - 15; i < width - 7; i++)
	{
		for (int j = 8; j < height-8; j++)
		{
			gfx.PutPixel(i, j, c);
		}
	}

	// for down wall
	for (int i = 8; i < width-8; i++)
	{
		for (int j = height - 14; j < height - 7; j++)
		{
			gfx.PutPixel(i, j, c);
		}
	}
}

int Board::GetDimension() const
{
	return dimension;
}
