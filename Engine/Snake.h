#pragma once

#include "Board.h"


class Snake{
private:
	class Segment {
	public: 
		void InitHead(const Location& loc);
		void InitBody(); 
		void Follow(const Segment& next);
		void Moveby(const Location& delta_loc);
		const Location& GetLocation() const;
		void Draw(Board& brd) const;
	private:
		Location loc;
		Color c;
	};


public: 
	Snake(const Location& loc);
	void MoveBy(const Location& delta_loc);
	Location GetNextHeadLocation(const Location delta_loc) const;
	void Grow();
	void Draw(Board& brd)const;
	bool IsInTileExceptEnd(Location& target)const;
	bool IsInTile(Location& target)const;

private:
	static constexpr Color headColor = Colors::Yellow;
	static constexpr Color bodyColor = Colors::Green;
	static constexpr int nSegmentMax = 100;
	Segment segments[nSegmentMax];
	int nSegment = 1;
};