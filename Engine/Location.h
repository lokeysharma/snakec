#pragma once
//basic object to hold the double int for board points.

class Location {

public:
	void Add(const Location val) {
		x += val.x;
		y += val.y;
	}
	//overloading operator to work with locations to compare between two locations.
	bool operator== (const Location& rhs) const{
		return x == rhs.x && y == rhs.y;
	}
	int x;
	int y;
};
